package seabattle.model.events;

import java.util.EventObject;

/*
 * Событие, связанное с результатом хода
 */
public class ResultOfShotEvent extends EventObject {
    /*
     * Some data string
     */
    public ResultOfShotEvent(Object source) {
        /* TODO some action*/
        super(source);
    }
}

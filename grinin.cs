﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 

//Comment for 1st commit master

namespace ServiceDB
{
    [Serializable]
    public class Card
    {
        public Header Header { get; set; }
        public List<Component> Components{ get; set; }
        public string Description
        {
            get { return Header.Number.ToString() + ". " + Header.Orderer.ToString() + " " + Header.Name.ToString(); }
        }

        public Card()
        {
            Header = new Header();
            Components = new List<Component>();
        }



       // Commit branch

        public override string ToString()
        {
            return Description;
        }
    }
}

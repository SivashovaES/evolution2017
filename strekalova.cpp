#include "routedatabase.h"
#include <QMessageBox>
#include <QString>

RouteDataBase::RouteDataBase()
{
	maxID=0;//������������ �������� ��������������
	modified=false;//���� ������ �� ��������
}
int RouteDataBase::count()const
{
	return ListOfRoute.size();
}
//������� ������ �  ���������������
void RouteDataBase::remove(unsigned int id)
{
	QList<minibus>::iterator it;
	bool find=false;
	int position=-1;
	for(it=ListOfRoute.begin();it!=ListOfRoute.end() && find==false; it++)
	{
		position++;
		find=it->id==id;
	}
	ListOfRoute.takeAt(position);
	modified=true;
}
//���������� ������ (������ ��� ������) �� ��������� ��������������
void RouteDataBase::record() const
{
	bool find=false;
	int position=-1;
	if(id>=0)
	{
		QList<minibus>::const_iterator it;
		for(it=ListOfRoute.constBegin(); it!=ListOfRoute.constEnd() && find==false; it++)
		{
			position++;
			find=it->id==id; 
			record=ListOfRoute.value(position);
		}
	}
}
int RouteDataBase::update(unsigned int id, const QString & name, const QString & value)
{
	minibus tmp;
	bool find=false;
	int position=0;
	//remove(tmp.id);//������� ������ �� ��������� ������������
	QList<minibus>::iterator it=ListOfRoute.begin();
	while(it!=ListOfRoute.end() && find==false)
	{
		//int tmp=it->id;
 		if(it->id==id)
		{
			find=true;
			//�������� ������ 
			if(name == "number")
			{
				it->number = value;
			}
			if(name=="middleStations")
			{
				it->middleStations=value;
			}
			if(name=="startStation")
			{
				if(value=="����������� �����")
					it->startStation=0;
				if(value=="������")
					it->startStation=1;
				if(value=="����������������� �����")
					it->startStation=2;
				if(value=="��������� ���������")
					it->startStation=3;
				if(value=="�����")
					it->startStation=4;
				if(value=="��������")
					it->startStation=5;
			}
			if(name=="endStation")
			{
				if(value=="����������� �����")
					it->endStation=0;
				if(value=="������")
					it->endStation=1;
				if(value=="����������������� �����")
					it->endStation=2;
				if(value=="��������� ���������")
					it->endStation=3;
				if(value=="�����")
					it->endStation=4;
				if(value=="��������")
					it->endStation=5;
			}
			if(name=="distrCost")
			{
				int perem=value.toInt();
				it->distrCost=perem;
			}
			if(name=="fullyCost")
			{
				int perem=value.toInt();
				it->fullyCost=perem;
			}
			if(name=="timeOfThePath")
			{
				QTime perem=QTime::fromString(value);
				it->timeOfThePath=perem;
			} 
			if(name=="oneDistrict")
			{
				bool flag=value=="true";
				it->oneDistrict=flag;
			}
			modified = true;
			QList<minibus>::iterator it1;
			//������� ������� ��� �������
			it1=ListOfRoute.begin();
			while (it1!= ListOfRoute.end() && *it1<*it)
			{
				position++;
				it1++;
			}
			//remove(it->id);
			//QMessageBox::warning(this,QString("�������� ������"), QString::number(it->id));
			//ListOfRoute.inserting(*it);
		}
		else
		{
			it++;
		}
	}
	return position;
}
//���������� ������ ����-���, ������� ������ ������������ � �������� � ������ ���������� �������
const QVector<minibus::Item> RouteDataBase::records() const
{
	QVector<minibus::Item> vector(0);
	QList<minibus>::const_iterator it=ListOfRoute.constBegin();
	while(it!=ListOfRoute.constEnd())
	{
		minibus::Item str=(*it).strBrowser;
		vector.append(str);//��������� ������
		it++;
	}
	return vector;
}

/*! �������� ���� ������
*/
void RouteDataBase::clear()
{
	//�������� ���� ������
	ListOfRoute.clear();
	//���� ������ ��������
	modified = true;
}
/*! �������� ������ � ���� ������ �������������� ����������
\param[in|out] tmp ����������� ������
\return - ������� �������
*/
int RouteDataBase::inserting(minibus &tmp)
{
	int insertPos = 0;
	QList<minibus>::iterator it;
	//������� ������� ��� �������
	it=ListOfRoute.begin();
	while (it!= ListOfRoute.end() && *it<tmp)
	{
		insertPos++;
	}
	//��������� ������ � ������
	//ListOfRoute.insert(insertPos,tmp);
	//��������� ��� ���� ������ ���� ��������
	modified = true;

	return insertPos;
};
bool RouteDataBase::save(QString fileName) 
{
	if(!fileName.isEmpty()) // ���� ��� ����� ������
	{
		//���� ������ �� ��������
		modified = false;
		//������� ����
		QFile MyFile(fileName);
		// ��������� ���� ������ ��� ������ � ������� ����� ��� ������ ������
		if(MyFile.open(QIODevice::WriteOnly))
		{
			QDataStream output(&MyFile);//����� ��� ������ ������
			//���������� ������ � �����
			output<<ListOfRoute;
			//��������� ����
			MyFile.close();
			//���� ��������
			return true;
		}
	}
	//���� �� ��������
	return false;
}
bool RouteDataBase::load(QString fileName)
{
	bool ok;//������� ���������� ��������
	//���� ��� ����� ������
	if(!fileName.isEmpty())
	{
		//������� ����
		QFile MyFile(fileName);
		// ��������� ���� ������ ��� ������ � ������� ����� ��� ������� ������
		if(MyFile.open(QIODevice::ReadWrite))
		{
			QDataStream output(&MyFile);//����� ��� ������� ������
			//������� ���� ������
			clear();
			maxID = -1;
			//������� ������ �� ������
			output >> ListOfRoute;
			ok = true;//������ ������� ���������
			modified = false;//���� ������ �� ��������
	
			minibus buf;
			QList<minibus>::iterator it;
			for(it=ListOfRoute.begin();it!=ListOfRoute.end();it++)
			{
				if(maxID < buf.id)
				{
					maxID = buf.id;
				}	
			}
		}
		else
			ok = false;//������ �� ���������
	}
	else 
		ok = false;//������ �� ���������
	//���������� ���������� ��������
	return ok;
}
/* ������� ������� ����, ���������� �� ���� ������ ����� �� ����������/��������
\return - �������
*/
bool RouteDataBase::isModified() const
{
    return modified;
}

RouteDataBase::~RouteDataBase()
{

}
